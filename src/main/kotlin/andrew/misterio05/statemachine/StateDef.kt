package andrew.misterio05.statemachine

import andrew.misterio05.statemachine.phases.Phase
import andrew.misterio05.statemachine.utils.Time

data class StateDef<C : StateContext>(
    var no: Int,
    var anim: String,
    var isCtrl: Boolean,
    var body: StateBody<C>,
) {
    private val timeField = Time()
    var time: Float
        set(value) = timeField.set(value)
        get() = timeField.value

    fun reset() {
        time = 0f
    }
}

typealias StateBody<C> = C.((Phase) -> Unit) -> Unit
