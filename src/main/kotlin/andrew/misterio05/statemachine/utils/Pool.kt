package andrew.misterio05.statemachine.utils

import com.badlogic.gdx.utils.Pool

operator fun <Type> Pool<Type>.invoke(): Type = this.obtain()

operator fun <Type> Pool<Type>.invoke(free: Type) = this.free(free)

inline fun <Type> pool(
    initialCapacity: Int = 16,
    max: Int = Int.MAX_VALUE,
    crossinline discard: (Type) -> Unit = {},
    crossinline provider: () -> Type,
): Pool<Type> = object : Pool<Type>(initialCapacity, max) {
    override fun newObject(): Type = provider.invoke()
    override fun discard(element: Type) = discard.invoke(element)
}
