package andrew.misterio05.statemachine.utils

class Time(time: Float = 0f) {

    var value: Float = time
        private set
    val dt: Float get() = value - oldValue
    internal var oldValue: Float = 0f

    override fun equals(other: Any?): Boolean = when (other) {
        is Float -> other == value || other < value && other > oldValue
        else -> super.equals(other)
    }

    fun set(newValue: Float) {
        oldValue = if (newValue < oldValue) newValue else value
        value = newValue
    }

    override fun hashCode(): Int {
        var result = value.hashCode()
        result = 31 * result + oldValue.hashCode()
        return result
    }

    override fun toString(): String = "Time(value=$value, dt=$dt)"
}

infix fun Time.eq(other: Float): Boolean = equals(other)

operator fun Time.compareTo(other: Float): Int = when {
    other < oldValue -> 1
    other > value -> -1
    else -> 0
}
