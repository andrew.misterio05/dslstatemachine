package andrew.misterio05.statemachine.utils

import andrew.misterio05.statemachine.StateContext

fun interface EntityToContextMapper<E> {

    operator fun invoke(entity: E): StateContext?
}