package andrew.misterio05.statemachine

import com.badlogic.gdx.utils.IntMap

fun <C : StateContext> states(
    createStateDef: () -> SyntaxStateDef = ::SyntaxStateDef,
    action: SyntaxStateMap<C, SyntaxStateDef>.() -> Unit,
): IntMap<StateDef<C>> = IntMap<StateDef<C>>().apply {
    SyntaxStateMap(statesMap = this, createSyntax = createStateDef).action()
}

fun <C : StateContext> createPermanentState(body: StateBody<C>) = StateDef(
    no = -1,
    anim = "",
    isCtrl = false,
    body = body,
)