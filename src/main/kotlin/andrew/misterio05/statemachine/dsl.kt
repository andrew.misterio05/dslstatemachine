package andrew.misterio05.statemachine

import andrew.misterio05.statemachine.phases.Phase
import com.badlogic.gdx.utils.IntMap

fun <T: StateContext> states(
    init: Int,
    action: SyntaxStateMap<T, SyntaxStateDef>.() -> Unit,
) = states(init, ::SyntaxStateDef, action)

fun <C: StateContext, S : SyntaxStateDef> states(
    init: Int,
    createSyntax: () -> S,
    action: SyntaxStateMap<C, S>.() -> Unit,
): StateHolder<C> {

    val states = IntMap<StateDef<C>>().apply {
        SyntaxStateMap(this, createSyntax).action()
    }

    return StateHolder(
        current = states[init],
        states = states,

    )
}

class SyntaxStateMap<C: StateContext, S : SyntaxStateDef>(
    private val statesMap: IntMap<StateDef<C>>,
    private val createSyntax: () -> S,
) {
    @StateDsl
    fun stateDef(
        no: Int,
        anim: String,
        isCtrl: Boolean = true,
        action: context(C, S) () -> Unit = {}
    ) {
        val syntax = createSyntax()
        statesMap.put(
            no,
            StateDef(
                no = no,
                anim = anim,
                isCtrl = isCtrl,
                body = {
                    syntax.apply {
                        addNewPhase = it
                        action(this@StateDef, this)
                    }
                },
            )
        )
    }
}

open class SyntaxStateDef {
    internal var addNewPhase: (Phase) -> Unit = empty

    fun make(phase: Phase) = addNewPhase(phase)

    companion object {
        private val empty: (Phase) -> Unit = {}
    }
}
