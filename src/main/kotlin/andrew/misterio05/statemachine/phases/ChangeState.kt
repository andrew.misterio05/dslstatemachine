package andrew.misterio05.statemachine.phases

import andrew.misterio05.statemachine.utils.invoke
import andrew.misterio05.statemachine.utils.pool


class ChangeState private constructor() : Phase {
    var nextStateNo: Int? = null
        private set

    companion object {
        private val Pool = pool(
            discard = { it.nextStateNo = null },
            provider = ::ChangeState,
        )

        operator fun invoke(no: Int): ChangeState = Pool().apply {
            nextStateNo = no
        }
    }
}
