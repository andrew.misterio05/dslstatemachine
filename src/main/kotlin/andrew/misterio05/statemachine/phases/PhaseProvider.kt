package andrew.misterio05.statemachine.phases

import kotlin.reflect.KClass

interface PhaseProvider<E> {
    operator fun get(klass: KClass<out Phase>): PhaseExecutionDelegate<E, Phase>
}
