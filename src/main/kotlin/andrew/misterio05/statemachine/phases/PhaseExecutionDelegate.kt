package andrew.misterio05.statemachine.phases

import andrew.misterio05.statemachine.utils.invoke
import com.badlogic.gdx.utils.Pool

fun interface PhaseExecutionDelegate<T, P : Phase> {
    operator fun invoke(entity: T, phase: P)
}

abstract class PhasePooledExecutionDelegate<T, P : Phase>(
    private val pool: Pool<P>,
): PhaseExecutionDelegate<T, P> {
    protected abstract fun internalExecute(entity: T, phase: P)

    override fun invoke(entity: T, phase: P) {
        internalExecute(entity, phase)
        pool.invoke(phase)
    }
}
