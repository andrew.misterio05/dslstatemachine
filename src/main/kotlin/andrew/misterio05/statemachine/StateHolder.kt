package andrew.misterio05.statemachine

import com.badlogic.gdx.utils.IntMap

class StateHolder<C : StateContext>(
    internal var current: StateDef<C>,
    internal val states: IntMap<StateDef<C>>,
    internal val permanentState: StateDef<C> = StateDef(-1, "", false, {}),
) {
    val state get() = current
}