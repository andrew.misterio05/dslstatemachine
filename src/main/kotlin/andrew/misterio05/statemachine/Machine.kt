package andrew.misterio05.statemachine

import andrew.misterio05.statemachine.phases.ChangeState
import andrew.misterio05.statemachine.phases.Phase
import andrew.misterio05.statemachine.phases.PhaseExecutionDelegate
import andrew.misterio05.statemachine.phases.PhaseProvider
import andrew.misterio05.statemachine.utils.EntityToContextMapper
import com.badlogic.gdx.utils.Array
import kotlin.reflect.KClass

class Machine<E>(
    private val phaseProvider: PhaseProvider<E>,
    private val mapEntityToContext: EntityToContextMapper<E>,
) {

    /** Actions on each phase of the state that are performed at the current time and added by a special condition.*/
    private val phasesToExecutionTmp = Array<Phase>()

    fun update(entity: E, stateHolder: StateHolder<StateContext>, dt: Float) {
        update(entity, stateHolder.current, stateHolder, dt)
        update(entity, stateHolder.permanentState, stateHolder, dt)
    }

    private fun update(
        entity: E,
        currentState: StateDef<StateContext>,
        stateHolder: StateHolder<StateContext>,
        dt: Float
    ) {
        phasesToExecutionTmp.clear()
        currentState.apply {
            time += dt
            body(mapEntityToContext(entity) ?: error("Entity has not context"), phasesToExecutionTmp::add)
        }
        phasesToExecutionTmp.forEach { phase ->
            when (phase) {
                is ChangeState -> stateHolder.apply {
                    current = states[phase.nextStateNo!!].apply (StateDef<*>::reset)
                }
                else -> phaseProvider[phase::class](entity = entity, phase = phase)
            }
        }
    }
}
