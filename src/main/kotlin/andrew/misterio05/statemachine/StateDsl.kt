package andrew.misterio05.statemachine

@DslMarker
@Target(AnnotationTarget.FUNCTION)
annotation class StateDsl
